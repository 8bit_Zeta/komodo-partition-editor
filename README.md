# Komodo Partition Editor

Cross-platform, modular partition editor. Currently written in Python and Bash.

Still very early development. Planned to be a module for the Fenix Installer for RebornOS.
