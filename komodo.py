#!/usr/bin/python

# needed for human readable partition size conversions
import math

# used to process and return BASH commands
import subprocess

# temporary for the use of matplotlib
import numpy as np
import matplotlib.pyplot as plt

def runBash(command):
    # takes a bash command in the form of a string, processes it, and returns the output
    return(subprocess.check_output(['bash','-c', command]))

def getDisks():
    # runs a parted command to return a list of the disks on system
    systemDisks=runBash("parted -l | grep /dev | awk '{print substr($2, 1, length($2)-1)}'")
    diskList = systemDisks.split()

    return diskList

def printUserChoices(diskList) -> None:
    # initialize a counter for the number of disks on the system
    diskCount = 0

    # print the disk name and appropriate number according to parted
    for i in diskList:
        print(str(diskCount) + ": " + i.decode('UTF-8'))
        diskCount = diskCount + 1

def getListofPartitions(testDisk):
    # using the previously chosen disk (using parted), returns a list of all partitions on disk (using size in bytes)
    return runBash("parted " + testDisk + " unit B print free | grep ' ' | sed -n 'H; /Number/,+1h; ${g;p;}'").splitlines()

def getListsofPartitionSizesandTypes(partitionList, testDisk):
    # initializes lists for the partition sizes and types
    sizeList = []
    typeList = []

    for i in partitionList:
        # grabs the 1st entry from the parted generated partition list
        partNum = i.split()[0].decode('UTF-8')

        # checks the last character from the partNum. If it is a B, then the entry is Free Space and has no type
        # simply grabs the size from the 3rd entry and sets the type to Free Space
        if partNum[-1] == 'B':
            partSize = i.split()[2].decode('UTF-8')
            partType = "Free Space"

        # if the last character isn't a B, then there is a partition with an actual filesystem
        # grabs the size from the 4th entry
        # uses lsblk with the disk name to get the partition name
        # uses lsblk with the partition name to get the partition type
        else:
            partSize = i.split()[3].decode('UTF-8')
            partName = runBash("lsblk " + testDisk + " -l -n -o NAME | grep '" + i.split()[0].decode('UTF-8') + "$' | awk 'length > " + str(len(testDisk[5:])) + "' | tr -d '\n'").decode('UTF-8')
            partType = runBash("lsblk -f /dev/" + partName + " -l -n -o FSTYPE").decode('UTF-8')

        # appends the size and type to their appropriate lists
        sizeList.append(partSize[:-1])
        typeList.append(partType)

    return sizeList, typeList

def getHumanReadablePartitionData(partSize, partType):
    # gets the human readable format for a partition (passes size in bytes and partition type name)
    return getHumanReadableSize(int(partSize)) + " " + partType

def getHumanReadableSize(sizeInBytes):
    # list of common partion size suffixes
    sizeSuffix = ("B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB")

    # try/except used to handle divide by zero
    try:
        # uses logarithmic function to determine the exponent to be used later.
        # using 1024 in correspondence with International System of Units (SI)
        # https://physics.nist.gov/cuu/Units/binary.html
        i = int(math.log(sizeInBytes, 1024))
    except:
        return "0 B"
    
    # using the exponent value from the logarithmic function,
    # we raise 1024 to it to get the divisor needed to simplify
    # bytes to a more readable format (kibibyte, mebibyte, etc.)
    exponentialDivisor = 1024 ** i

    # divides the number of bytes by our previously determined divisor
    # and rounds to 2 decimal places for simplicity
    s = round(sizeInBytes / exponentialDivisor, 2)

    return str(s) + " " + sizeSuffix[i]

def createChart(sizes, types) -> None:

    # most information here pulled from:
    # https://matplotlib.org/3.1.1/gallery/pie_and_polar_charts/pie_and_donut_labels.html#sphx-glr-gallery-pie-and-polar-charts-pie-and-donut-labels-py

    # creates subplots for the chart
    fig, ax = plt.subplots(figsize=(6, 3), subplot_kw=dict(aspect="equal"))

    # set figure background opacity (alpha) to 0
    fig.patch.set_alpha(0.)

    # creates wedges for the charts using the sizes we gathered from the partitions lists (need to create a minimum size or some form of scaling)
    # sets the wedges width (makes it a ring)
    # sets the wedges to be ordered in a clockwise fashion
    # sets the starting point to be straight up (90 degrees)
    wedges, texts = ax.pie(sizes, wedgeprops=dict(width=0.33), counterclock=False, startangle=90)

    # sets the label box parameteres
    bbox_props = dict(boxstyle="square,pad=0.5", fc="w", ec="k", lw=0.72)

    # sets the arrows that connect the wedges and label boxes
    kw = dict(arrowprops=dict(arrowstyle="-"),
            bbox=bbox_props, zorder=0, va="center")

    for i, p in enumerate(wedges):
        ang = (p.theta2 - p.theta1)/2. + p.theta1
        y = np.sin(np.deg2rad(ang))
        x = np.cos(np.deg2rad(ang))
        horizontalalignment = {-1: "right", 1: "left"}[int(np.sign(x))]
        connectionstyle = "angle,angleA=0,angleB={}".format(ang)
        kw["arrowprops"].update({"connectionstyle": connectionstyle})

        # gets the titles for the wedges in a human readable format
        wedgeTitle = getHumanReadablePartitionData(sizes[i], types[i])

        # sets the annotation of the wedges using the human readable title, label parameters, and arrow styles
        ax.annotate(wedgeTitle, xy=(x, y), xytext=(1.35*np.sign(x), 1.4*y),
                    horizontalalignment=horizontalalignment, **kw)

    # sets a blank title for the entire chart
    ax.set_title(" ")


def main():
    # method call to get a list of available disks on system
    diskList = getDisks()

    # prints a list of the disks to terminal for user selection (ex. /dev/nvme0n1)
    printUserChoices(diskList)

    # asks user for a which disk they want to analyze (uses a integer in a list) saves the name of the actual disk to a variable
    diskChoice = input("Please enter the number of the disk you would like to view: ")
    testDisk = diskList[int(diskChoice)].decode('UTF-8')

    # gets a list of all partitions on the selected disk
    partList = getListofPartitions(testDisk)

    # gets a list of the sizes (in bytes) and types (filesystem) from the previous partition list
    sizes, types = getListsofPartitionSizesandTypes(partList, testDisk)

    # uses matplotlib to create a visual chart and displays it
    createChart(sizes, types)
    plt.show()


if __name__ == "__main__":
    main()
